<?php declare(strict_types=1);

// namespace SwtCustomFields\Service;


/**
 * Class SwtCustomFieldSetService
 *
 * @version   1.0.0
 * @since     1.0.0
 * @author    Silvio Kurth <s.kurth@agentur-sowhat.de>
 * @copyright Copyright (c) SOWHAT! c/o Websling GmbH
 */
class SwtCustomFieldSetService
{

    /**
     * @var \Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface
     */
    private $customFieldSetRepository;

    public function __construct(\Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface $customFieldSetRepository)
    {
        $this->customFieldSetRepository = $customFieldSetRepository;

        $this->customFieldSetRepository->create([
            [
                'name' => 'swag_example',
                'customFields' => [
                    ['name' => 'swag_example_size', 'type' => CustomFieldTypes::INT],
                    ['name' => 'swag_example_color', 'type' => CustomFieldTypes::TEXT]
                ]
            ]
        ], \Shopware\Core\Framework\Context::createDefaultContext());
    }
}