<?php declare(strict_types=1);

namespace SwtCustomFields\Struct;

use Shopware\Core\Framework\Struct\Struct;

class CustomStruct extends Struct
{
    public $test = 'foo';
}