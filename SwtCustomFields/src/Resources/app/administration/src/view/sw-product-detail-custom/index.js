import { Component } from 'src/core/shopware';
import template from './sw-product-detail-extended.html.twig';

Component.register('sw-product-detail-extended', {
    template,

    metaInfo() {
        return {
            title: 'Extended'
        };
    },
});