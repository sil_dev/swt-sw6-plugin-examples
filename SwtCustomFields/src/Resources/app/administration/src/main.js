import './view/sw-product-detail-extended';
import './page/sw-product-detail';

import { Module } from 'src/core/shopware';

Module.register('sw-new-tab-extended', {
    routeMiddleware(next, currentRoute) {
        if (currentRoute.name === 'sw.product.detail') {
            currentRoute.children.push({
                name: 'sw.product.detail.extended',
                path: '/sw/product/detail/:id/extended',
                component: 'sw-product-detail-extended',
                meta: {
                    parentPath: "sw.product.index"
                }
            });
        }
        next(currentRoute);
    }
});