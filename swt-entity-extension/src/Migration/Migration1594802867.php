<?php

declare(strict_types=1);

namespace Swt\EntityExtension\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;
use Shopware\Core\Framework\Migration\InheritanceUpdaterTrait;

/**
 * Migration1594802867 class
 */
class Migration1594802867 extends MigrationStep
{
    use InheritanceUpdaterTrait;

    /**
     * @return integer
     */
    public function getCreationTimestamp(): int
    {
        return 1594802867;
    }

    /**
     * @param Connection $connection
     *
     * @return void
     */
    public function update(Connection $connection): void
    {
        // implement update
        $columns = array_keys($connection->getSchemaManager()->listTableColumns('product'));
        if (!in_array('product_manager', $columns)) {
            $this->updateInheritance($connection, 'product', 'product_manager');
        }
    }

    /**
     * @param Connection $connection
     *
     * @return void
     */
    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
