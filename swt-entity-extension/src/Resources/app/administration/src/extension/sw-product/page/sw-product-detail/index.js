const { Component } = Shopware;
const { Criteria } = Shopware.Data;

Component.override('sw-product-detail', {

    watch: {
        'product.extensions.product_manager.id': {
            handler(val, oldVal) {
                console.log(oldVal);
                console.log(val);
            }
        }
    },

    computed: {
        productCriteria() {
            const criteria = this.$super('productCriteria');
            criteria.addAssociation('product_manager');
            return criteria;
        },
    },
});