<?php

declare(strict_types=1);

namespace Swt\EntityExtension\Subscriber;

use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Shopware\Core\Content\Product\ProductEvents;
use Shopware\Core\Framework\Api\OAuth\User\User;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Storefront\Page\Product\ProductLoaderCriteriaEvent;

/**
 * MySubscriber class
 */
class MySubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityRepositoryInterface
     */
//    private $userRepository;
//
    // /**
    //  * @param EntityRepositoryInterface $userRepository
    //  */
    // public function __construct(EntityRepositoryInterface $userRepository)
    // {
    //     $this->userRepository = $userRepository;
    // }

//    /**
//     * @return array
//     */
//    public static function getSubscribedEvents(): array
//    {
//        return [
//            ProductEvents::PRODUCT_LOADED_EVENT => 'onProductsLoaded',
//        ];
//    }
//
//    /**
//     * @param EntityLoadedEvent $event
//     *
//     * @return void
//     */
//    public function onProductsLoaded(EntityLoadedEvent $event): void
//    {
//        /** @var ProductEntity $productEntity */
//        foreach ($event->getEntities() as $productEntity) {
//
//            // $context = \Shopware\Core\Framework\Context::createDefaultContext();
//            // $criteria = new Criteria();
//            // $criteria->addFilter(new EqualsFilter('id', $productEntity->get('product_manager')));
//            // /** @var UserEntity|null $user */
//            // $user = $this->userRepository->search($criteria, $context)->getEntities()->first();
//            // $user = $this->userRepository->search(new Criteria(), $context)->getEntities();
//
//            // $productManager = $productEntity->getExtension('product_manager');
//            // if ($productManager == null) {
//            //     $productManager = new \Shopware\Core\Framework\Struct\Struct()
//            // }
//
//            // $productEntity->addExtension('product_manager', $productManager);
//        }
//    }

    public static function getSubscribedEvents(): array
    {
        return [
            ProductLoaderCriteriaEvent::class => 'onProductCriteriaLoaded',
        ];
    }

    public function onProductCriteriaLoaded(ProductLoaderCriteriaEvent $event): void
    {
        $event->getCriteria()->addAssociation('bundles.products.cover');
    }
}
