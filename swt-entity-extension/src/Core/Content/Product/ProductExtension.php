<?php

declare(strict_types=1);

namespace Swt\EntityExtension\Core\Content\Product;

use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\System\User\UserDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityExtension;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Inherited;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Extension;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;

/**
 * CustomExtension class
 */
class ProductExtension extends EntityExtension
{
    /**
     * @param FieldCollection $collection
     *
     * @return void
     */
    public function extendFields(FieldCollection $collection): void
    {
        $collection->add(
            (new ManyToOneAssociationField(
                'product_manager',
                'productManager',
                UserDefinition::class,
                'id',
                true
            ))->addFlags(new Inherited(), new Extension(), new Required())
        );
    }

    public function extendCriteria(Criteria $criteria): void
    {
        $criteria->addAssociation('product_manager');
    }

    /**
     * @return string
     */
    public function getDefinitionClass(): string
    {
        return ProductDefinition::class;
    }
}
