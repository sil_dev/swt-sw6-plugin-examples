import template from './sw-migration-dashboard-card.html.twig';
import './sw-migration-dashboard-card.scss';

const { Component } = Shopware;

Component.register('swag-migration-dashboard-card', {
    template
});
